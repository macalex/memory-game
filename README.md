# Memory Game

## Getting started

### Install dependencies
Install the project with the following command:

``yarn``

Then you can start the application in develop or production mode

### Start app

**Develop**

``yarn dev``

**Production**

``yarn build``