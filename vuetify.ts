import { mdi } from 'vuetify/lib/iconsets/mdi'
import { aliases, fa } from 'vuetify/lib/iconsets/fa'
// @ts-ignore
import colors from 'vuetify/lib/util/colors'

export default {
  icons: {
    defaultSet: 'fa',
    aliases,
    sets: {
      fa,
      mdi,
    },
  },
  theme: {
    defaultTheme: 'light',
    themes: {
      light: {
        dark: false,
        colors: {
          primary: colors.grey.darken3,
        },
      },
      dark: {
        dark: true,
        colors: {
          primary: colors.grey.darken3,
        },
      },
    },
  },
}
