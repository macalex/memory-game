import path from 'path'
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vuetify from 'vite-plugin-vuetify'
import svgLoader from 'vite-svg-loader'

export default defineConfig({
  build: {
    target: 'esnext',
  },
  plugins: [
    vue(),
    svgLoader({
      defaultImport: 'url', // or 'raw'
    }),
    vuetify({
      autoImport: true,
    }),
  ],
  define: { 'process.env': { ...process.env, ...loadEnv('development', process.cwd()) } },
  resolve: {
    alias: {
      '#': __dirname,
      '@': path.resolve(__dirname, 'src'),
      '~': path.resolve(__dirname, 'src/components'),
    },
    extensions: ['.js', '.json', '.jsx', '.mjs', '.ts', '.tsx', '.vue'],
  },
  css: {
    preprocessorOptions: {
      sass: {
        additionalData: `
          @import "@/styles/_variables.sass"
        `,
      },
    },
  },
})
