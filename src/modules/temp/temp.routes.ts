import { RouteRecordRaw } from 'vue-router'

export const accountRoutes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () => import('@/modules/temp/views/layouts/DefaultLayout.vue'),
    children: [
      {
        path: '/',
        name: 'dashboard',
        component: () => import('@/modules/temp/views/pages/DashboardPage.vue'),
      },
    ],
  },
]

export default accountRoutes
