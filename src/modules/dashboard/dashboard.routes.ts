import { RouteRecordRaw } from 'vue-router'

export const accountRoutes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () => import('@/modules/dashboard/views/layouts/DefaultLayout.vue'),
    children: [
      {
        path: '/',
        name: 'dashboard',
        component: () => import('@/modules/dashboard/views/pages/DashboardPage.vue'),
      },
    ],
  },
]

export default accountRoutes
