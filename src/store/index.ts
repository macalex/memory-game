import { createStore } from 'vuex'
import { Card } from '@/services/DeckService'

export type Round = {
  first: Card
  second: Card
  round: number
  equal: boolean
}

const state: {
  history: Round[]
  first: Card | null
  second: Card | null
  round: number
} = {
  history: [],
  first: null,
  second: null,
  round: 1,
}

export default createStore({
  state,
  getters: {
    getFirst(state): Card | null {
      return state.first
    },
    getSecond(state): Card | null {
      return state.second
    },
    getRound(state): number {
      return state.round
    },
    countEqual(state): number {
      return state.history.filter((round) => round.equal).length
    },
    countHistory(state): number {
      return state.history.length
    },
  },
  mutations: {
    ADD_TO_HISTORY: (state, payload: Round) => {
      if (payload) {
        state.history.push(payload)
      }
    },
    SET_FIRST: (state, payload: Card) => {
      if (payload || payload === null) {
        state.first = payload
      }
    },
    SET_SECOND: (state, payload: Card) => {
      if (payload || payload === null) {
        state.second = payload
      }
    },
    SET_ROUND: (state, payload: number) => {
      if (payload || payload === 0) {
        state.round = payload
      }
    },
    NEXT_ROUND: (state) => {
      state.round++
    },
    RESET_ROUND: (state) => {
      state.first = null
      state.second = null
    },
    RESET_HISTORY: (state) => {
      state.history = []
      state.first = null
      state.second = null
      state.round = 1
    },
  },
  actions: {
    addToHistory({ commit }, payload): void {
      commit('ADD_TO_HISTORY', payload)
    },
  },
  modules: {},
})
