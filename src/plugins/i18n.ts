import { createI18n } from 'vue-i18n'

const messages = {
  en: await import('@/locales/en.json'),
  hu: await import('@/locales/hu.json'),
}

export const i18n = createI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages,
})

export default i18n
