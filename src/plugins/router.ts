import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import mainRoutes from '@/routes'
import dashboardRoutes from '@/modules/dashboard/dashboard.routes'

const routes: Array<RouteRecordRaw> = [...mainRoutes, ...dashboardRoutes]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router
