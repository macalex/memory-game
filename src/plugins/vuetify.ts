// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'
import '@/styles/main.sass'

// Vuetify
import { createVuetify } from 'vuetify'
import vuetify from '#/vuetify'

export default createVuetify(vuetify)
