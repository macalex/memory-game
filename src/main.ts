import { createApp } from 'vue'
import App from '@/App.vue'
import i18n from '@/plugins/i18n'
import router from '@/plugins/router'
import store from '@/store'
import vuetify from '@/plugins/vuetify'
import { loadFonts } from '@/plugins/webfontloader'

import filters from '@/filters/filters'

loadFonts()

const app = createApp(App)

app.config.globalProperties.$filters = filters

app.use(router).use(store).use(i18n).use(vuetify).mount('#app')
