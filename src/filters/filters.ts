import stringFilters from '@/filters/string.filters'

export default {
  ...stringFilters,
}
