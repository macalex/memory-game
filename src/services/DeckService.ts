export type Card = {
  num: number
  index: number
  src: string
  turnedOver: boolean
  show: boolean
}

export class DeckService {
  generate(): { [key: string]: any } {
    return [...Array(20).keys()]
  }

  shuffle(arr: { [key: string]: any }): { [key: string]: any } {
    if (arr.length) {
      for (let i = arr.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1))
        const temp = arr[i]
        arr[i] = arr[j]
        arr[j] = temp
      }
    }
    return arr
  }

  addCardPath(arr: { [key: string]: any }): Card[] {
    return arr.map((item: number, i: number) => {
      const num: number = item > 9 ? item - 10 : item
      return {
        num,
        index: i,
        src: `/src/assets/cards/card${num}.svg`,
        turnedOver: false,
        show: true,
      }
    })
  }

  getNewDeck(): Card[] {
    let arr = this.generate()
    arr = this.shuffle(arr)
    return this.addCardPath(arr)
  }
}

export default DeckService
